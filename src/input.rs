use std::collections::hash_map::Iter;
use std::collections::HashMap;

use glutin::{DeviceEvent, ElementState, Event, EventsLoop, KeyboardInput};

#[derive(PartialEq, Hash, FromPrimitive)]
pub enum Key {
	LCtr = 29,
	LWin = 125,
	LAlt = 56,
	Y = 21,
	U = 22,
	I = 23,
	O = 24,
	H = 35,
	J = 36,
	K = 37,
	L = 38,
	_7 = 8,
	_8 = 9,
	_9 = 10
}

impl Eq for Key {}

pub struct InputHandler(HashMap<Key, bool>);

impl InputHandler {
	pub fn new() -> Self {
		let mut key_map = HashMap::new();
		for key in vec![
			Key::LCtr, Key::LWin, Key::LAlt,
			Key::Y,    Key::U,    Key::I,
			Key::O,    Key::H,    Key::J,
			Key::K,    Key::L,    Key::_7,
			Key::_8,   Key::_9
		]
		{
			key_map.insert(key, false);
		}
		InputHandler(key_map)
	}

	pub fn is_down(&self, key: Key) -> bool {
		assert!(self.0.contains_key(&key));
		self.0[&key]
	}

	pub fn iter(&self) -> Iter<Key, bool> {
		self.0.iter()
	}

	pub fn read_events(&mut self, events: &mut EventsLoop) {
		events.poll_events(|e| {
			if let Event::DeviceEvent { event, .. } = e {
				if let DeviceEvent::Key(input) = event {
					self.process(input);
				}
			}
		});
	}

	fn process(&mut self, input: KeyboardInput) {
		if let Some(key) = num::FromPrimitive::from_u32(input.scancode) {
			match input.state {
				ElementState::Pressed => self.0.insert(key, true),
				ElementState::Released => self.0.insert(key, false),
			};
		}
	}
}