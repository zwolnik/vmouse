use std::{thread, time::Duration};

use enigo::{Enigo, MouseButton, MouseControllable};
use vecmat::vec::Vec2; 
use glutin::EventsLoop;

use crate::Display;
use crate::vec2ext::*;
use crate::input::*;

struct Config {
	max_speed: f64,
	acc: Vec2<f64>,
	friction: f64,
}

enum Presets {
	Standard(Config),
	Boosted(Config)
}

struct VirtualMouse {
	scroll: i32,
	conf: Presets,
	vel: Vec2<f64>,
	buttons_down: (bool, bool),
	buttons_flags: (bool, bool),
	displays: Vec<Display>,
	jump1: bool,
	jump2: bool,
	jump3: bool
}

pub struct DeviceEmulator {
	device: Enigo,
	vmouse: VirtualMouse,
	inputs: InputHandler,
	system_events: EventsLoop,
}

impl Presets {
	fn standard() -> Self {
		Presets::Standard(Config {
			max_speed: 10.,
			acc: Vec2::<f64>::from_array([3., 3.]),
			friction: 0.8
		})
	}

	fn boosted() -> Self {
		Presets::Boosted(Config {
			max_speed: 30.,
			acc: Vec2::<f64>::from_array([20., 20.]),
			friction: 0.8
		})
	}

	fn acc(&self) -> Vec2<f64> {
		use Presets::*;
		match self {
			Boosted(conf) | Standard(conf) => conf.acc
		}
	}

	fn max_speed(&self) -> f64 {
		use Presets::*;
		match self {
			Boosted(conf) | Standard(conf) => conf.max_speed
		}
	}

	fn friction(&self) -> f64 {
		use Presets::*;
		match self {
			Boosted(conf) | Standard(conf) => conf.friction
		}
	}
}

impl VirtualMouse {
	fn new(displays: Vec<Display>) -> Self {
		VirtualMouse {
			scroll: 0,
			vel: Vec2::new(),
			conf: Presets::standard(),
			buttons_down: (false, false),
			buttons_flags: (false, false),
			displays,
			jump1: false,
			jump2: false,
			jump3: false
		}
	}

	fn serve(&mut self, inputs: &InputHandler) {
		if inputs.is_down(Key::LWin) && inputs.is_down(Key::LCtr) {
			if inputs.is_down(Key::LAlt) {
				self.conf = Presets::boosted();
			} else {
				self.conf = Presets::standard();
			}
			for (key, value) in inputs.iter() {
				if !value {
					match key {
						&Key::U => self.buttons_down.0 = false,
						&Key::O => self.buttons_down.1 = false,
						&Key::_7 => self.jump1 = false,
						&Key::_8 => self.jump2 = false,
						&Key::_9 => self.jump3 = false,
						_ => (),
					}
					continue;
				}
				match key {
					&Key::J => self.vel[0] -= self.conf.acc()[0],
					&Key::L => self.vel[0] += self.conf.acc()[0],
					&Key::I => self.vel[1] -= self.conf.acc()[1],
					&Key::K => self.vel[1] += self.conf.acc()[1],
					&Key::U => {
						self.buttons_down.0 = true;
						self.buttons_flags.0 = true;
					}
					&Key::O => {
						self.buttons_down.1 = true;
						self.buttons_flags.1 = true;
					}
					&Key::Y => self.scroll += 1,
					&Key::H => self.scroll -= 1,
					&Key::_7 => self.jump1 = true,
					&Key::_8 => self.jump2 = true,
					&Key::_9 => self.jump3 = true,
					_ => (),
				}
			}
		}
		apply_friction(&mut self.vel, self.conf.friction());
		self.vel = limit(&self.vel, self.conf.max_speed());
	}

	fn reflect(&mut self, device: &mut Enigo) {
		device.mouse_move_relative(
			self.vel[0] as i32,
			self.vel[1] as i32
		);
		if self.buttons_down.0 {
			device.mouse_down(MouseButton::Left);
		} else if self.buttons_flags.0 {
			device.mouse_up(MouseButton::Left);
			self.buttons_flags.0 = false;
		}
		if self.buttons_down.1 {
			device.mouse_down(MouseButton::Right);
		} else if self.buttons_flags.1 {
			device.mouse_up(MouseButton::Right);
			self.buttons_flags.1 = false;
		}
		if self.scroll != 0 {
			device.mouse_scroll_y(self.scroll);
			self.scroll = 0;
		}
		if self.jump1 {
			let d = self.displays.get(0);
			if let Some(display) = d {
				let pos = display.center_pos_global();
				device.mouse_move_to(pos.0 as i32, pos.1 as i32);
			}
		}
		if self.jump2 {
			let d = self.displays.get(1);
			if let Some(display) = d {
				let pos = display.center_pos_global();
				device.mouse_move_to(pos.0 as i32, pos.1 as i32);
			}
		}
		if self.jump3 {
			let d = self.displays.get(2);
			if let Some(display) = d {
				let pos = display.center_pos_global();
				device.mouse_move_to(pos.0 as i32, pos.1 as i32);
			}
		}
	}
}

impl DeviceEmulator {
	pub fn new(displays: Vec<Display>) -> Self {
		DeviceEmulator {
			device: Enigo::new(),
			vmouse: VirtualMouse::new(displays),
			inputs: InputHandler::new(),
			system_events: EventsLoop::new(),
		}
	}

	pub fn run(&mut self) {
		loop {
			self.inputs.read_events(&mut self.system_events);
			self.vmouse.serve(&self.inputs);
			self.vmouse.reflect(&mut self.device);
			thread::sleep(Duration::from_millis(30));
		}
	}
}