#[macro_use]
extern crate num_derive;
extern crate regex;

mod input;
mod virtual_mouse;
mod vec2ext;

use std::process::{Command, Stdio};
use std::io::{Read, Write};
use std::str;

use virtual_mouse::DeviceEmulator;
use regex::Regex;

pub struct Display {
	width: u32,
	height: u32,
	xoffset: u32,
	yoffset: u32
}

impl Display {
	pub fn new(width: u32, height: u32, xoffset: u32, yoffset: u32) -> Self {
		Display {
			width,
			height,
			xoffset,
			yoffset
		}
	}

	pub fn center_pos_global(&self) -> (u32, u32) {
		let x = self.width/2 + self.xoffset;
		let y = self.height/2 + self.yoffset;
		(x, y)
	}
}

fn main() {

	let mut cmd_xrandr = Command::new("xrandr")
        .stdout(Stdio::piped())
        .spawn()
        .unwrap();

    let mut cmd_grep = Command::new("grep")
        .arg(" connected")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()
        .unwrap();

    if let Some(ref mut stdout) = cmd_xrandr.stdout {
        if let Some(ref mut stdin) = cmd_grep.stdin {
            let mut buf: Vec<u8> = Vec::new();
            stdout.read_to_end(&mut buf).unwrap();
            stdin.write_all(&buf).unwrap();
        }
    }

    let res = cmd_grep.wait_with_output().unwrap().stdout;
	let re = Regex::new(r"(\d{3,4})x(\d{3,4})\+(\d{1,4})\+(\d{1,4})").unwrap();

	let mut displays = Vec::<Display>::new();

	for cap in re.captures_iter(str::from_utf8(&res).unwrap()) {
		displays.push(
			Display::new(
				cap[1].parse().unwrap(),
				cap[2].parse().unwrap(),
				cap[3].parse().unwrap(),
				cap[4].parse().unwrap()			
			)
		) 
	}
	let mut emulator = DeviceEmulator::new(displays);
	emulator.run();
}