use vecmat::vec::Vec2;
use num_traits::{Float, Num};

pub fn limit<T>(vec: &Vec2<T>, upper_bound: T) -> Vec2<T>
where
    T: Sized + Num + Float,
{
    if vec.length() >= upper_bound {
        vec.normalize() * upper_bound
    } else {
        *vec
    }
}

pub fn apply_friction<T>(vec: &mut Vec2<T>, friction: T)
where
    T: Sized + Num + Float,
{
    *vec *= friction;
    if let (Some(lhs), Some(rhs)) = (vec.length().to_f64(), friction.to_f64()) {
        if lhs < 1. - rhs {
            *vec = Vec2::<T>::zero();
        }
    }
}
